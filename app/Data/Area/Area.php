<?php

namespace App\Data\Area;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use Relationship;
    
    protected $table = 'areas';

    protected $fillable = [
        'nome'
    ];
}
