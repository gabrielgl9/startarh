<?php

namespace App\Data\Area;

trait Relationship
{
    public function formacoes()
    {
        return $this->hasMany('App\Data\Formacao\Formacao', 'instituicao_id');
    }
}
