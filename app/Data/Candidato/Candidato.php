<?php

namespace App\Data\Candidato;

use Illuminate\Database\Eloquent\Model;

class Candidato extends Model
{
    use Relationship;
    
    protected $table = 'candidatos';

    protected $fillable = [
        'nome',
        'data_nascimento',
        'telefone',
        'email',
        'naturalidade',
        'endereco_id',
        'nacionalidade_id'
    ];
}
