<?php

namespace App\Data\Candidato;

trait Relationship
{
    public function nacionalidade()
    {
        return $this->belongsTo('App\Data\Nacionalidade\Nacionalidade', 'nacionalidade_id');
    }

    public function endereco()
    {
        return $this->belongsTo('App\Data\Endereco\Endereco', 'endereco_id');
    }

    public function formacoes()
    {
        return $this->hasMany('App\Data\Formacao\Formacao', 'candidato_id');
    }

    public function experiencias()
    {
        return $this->hasMany('App\Data\Experiencia\Experiencia', 'candidato_id');
    }

    public function competencias()
    {
        return $this->hasMany('App\Data\Competencia\Competencia', 'candidato_id');
    }
}
