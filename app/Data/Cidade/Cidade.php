<?php

namespace App\Data\Cidade;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    use Relationship;

    protected $table = 'cidades';
}
