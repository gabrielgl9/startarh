<?php

namespace App\Data\Cidade;

trait Relationship
{
    public function enderecos()
    {
        return $this->hasMany('App\Data\Endereco\Endereco', 'endereco_id');
    }

    public function estado()
    {
        return $this->belongsTo('App\Data\Estado\Estado', 'estado_id');
    }
}
