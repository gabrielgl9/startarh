<?php

namespace App\Data\Competencia;

use Illuminate\Database\Eloquent\Model;

class Competencia extends Model
{
    use Relationship;

    protected $table = 'competencias';

    protected $fillable = [
        'nivel',
        'anos_experiencia',
        'habilidade_id',
        'candidato_id'
    ];
}
