<?php

namespace App\Data\Competencia;

trait Relationship
{
    public function links()
    {
        return $this->hasMany('App\Data\Link\Link', 'competencia_id');
    }

    public function habilidade()
    {
        return $this->belongsTo('App\Data\Habilidade\Habilidade', 'habilidade_id');
    }

    public function candidato()
    {
        return $this->belongsTo('App\Data\Candidato\Candidato', 'candidato_id');
    }
}
