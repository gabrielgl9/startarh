<?php

namespace App\Data\Competencia;

use Illuminate\Http\Resources\Json\JsonResource;

class Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nivel' => $this->nivel,
            'anos_experiencia' => $this->anos_experiencia,
            'candidato_id' => $this->candidato_id,
            'habilidade_id' => $this->habilidade_id        
        ];
    }
}
