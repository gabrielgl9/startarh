<?php

namespace App\Data\Empresa\Empresa;

trait Relationship
{
    public function experiencias()
    {
        return $this->hasMany('App\Data\Experiencia\Experiencia', 'experiencia_id');
    }
}
