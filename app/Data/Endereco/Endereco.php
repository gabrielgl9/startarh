<?php

namespace App\Data\Endereco;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    use Relationship;

    protected $table = 'enderecos';

    protected $fillable = [
        'cep',
        'bairro',
        'logradouro',
        'numero',
        'complemento',
        'cidade_id'
    ];
}
