<?php

namespace App\Data\Endereco;

trait Relationship
{
    public function candidato()
    {
        return $this->hasMany('App\Data\Candidato\Candidato', 'candidato_id');
    }

    public function cidade()
    {
        return $this->belongsTo('App\Data\Cidade\Cidade', 'cidade_id');
    }
}
