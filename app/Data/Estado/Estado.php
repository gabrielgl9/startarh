<?php

namespace App\Data\Estado;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    use Relationship;

    protected $table = 'estados';
}
