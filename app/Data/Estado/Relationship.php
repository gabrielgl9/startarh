<?php

namespace App\Data\Estado;

trait Relationship
{
    public function cidades()
    {
        return $this->hasMany('App\Data\Cidade\Cidade', 'cidade_id');
    }

    public function regiao()
    {
        return $this->belongsTo('App\Data\Regiao\Regiao', 'cidade_id');
    }
}
