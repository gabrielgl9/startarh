<?php

namespace App\Data\Experiencia;

use Illuminate\Database\Eloquent\Model;

class Experiencia extends Model
{
    protected $table = 'experiencias';

    protected $fillable = [
        'data_inicial',
        'data_final',
        'status',
        'observacao',
        'profissao_id',
        'empresa_id',
        'candidato_id'
    ];
}
