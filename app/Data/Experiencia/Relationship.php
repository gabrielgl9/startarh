<?php

namespace App\Data\Experiencia;

trait Relationship
{
    public function profissao()
    {
        return $this->belongsTo('App\Data\Profissao\Profissao', 'profissao_id');
    }

    public function empresa()
    {
        return $this->belongsTo('App\Data\Empresa\Empresa', 'empresa_id');
    }

    public function candidato()
    {
        return $this->belongsTo('App\Data\Candidato\Candidato', 'candidato_id');
    }
}
