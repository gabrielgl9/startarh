<?php

namespace App\Data\Formacao;

use Illuminate\Database\Eloquent\Model;

class Formacao extends Model
{
    use Relationship;

    protected $table = 'formacoes';

    protected $fillable = [
        'data_inicial',
        'data_final',
        'area_id',
        'instituicao_id',
        'nivel_id',
        'status_id',
        'candidato_id'
    ];
}
