<?php

namespace App\Data\Formacao;

trait Relationship
{
    public function candidato()
    {
        return $this->belongsTo('App\Data\Candidato\Candidato', 'candidato_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Data\Status\Status', 'status_id');
    }

    public function nivel()
    {
        return $this->belongsTo('App\Data\Nivel\Nivel', 'nivel_id');
    }

    public function instituicao()
    {
        return $this->hasMany('App\Data\Instituicao\Instituicao', 'instituicao_id');
    }
}
