<?php

namespace App\Data\Formacao;

use Illuminate\Http\Resources\Json\JsonResource;

class Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'data_inicial' => $this->data_inicial,
            'data_final' => $this->data_final,
            'status_id' => $this->status_id,
            'area_id' => $this->area_id,
            'instituicao_id' => $this->instituicao_id,
            'nivel_id' => $this->nivel_id,
            'candidato_id' => $this->candidato_id,
        ];
    }
}
