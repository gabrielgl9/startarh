<?php

namespace App\Data\Habilidade;

use Illuminate\Database\Eloquent\Model;

class Habilidade extends Model
{
    use Relationship;
    
    protected $table = 'habilidades';

    protected $fillable = [
        'nome'
    ];
}
