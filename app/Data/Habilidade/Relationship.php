<?php

namespace App\Data\Habilidade;

trait Relationship
{
    public function Habilidade()
    {
        return $this->hasMany('App\Data\Competencia\Competencia', 'habilidade_id');
    }
}
