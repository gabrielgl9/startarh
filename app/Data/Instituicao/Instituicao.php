<?php

namespace App\Data\Instituicao;

use Illuminate\Database\Eloquent\Model;

class Instituicao extends Model
{
    use Relationship;

    protected $table = 'Instituicoes';

    protected $fillable = [
        'nome',
        'sigla'
    ];
}
