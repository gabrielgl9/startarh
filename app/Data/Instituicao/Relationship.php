<?php

namespace App\Data\Instituicao;

trait Relationship
{
    public function formacoes()
    {
        return $this->hasMany('App\Data\Formacao\Formacao', 'instituicao_id');
    }
}
