<?php

namespace App\Data\Link;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use Relationship;
    
    protected $table = 'links';

    protected $fillable = [
        'url',
        'competencia_id'
    ];
}
