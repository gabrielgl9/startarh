<?php

namespace App\Data\Link;

trait Relationship
{
    public function competencia()
    {
        return belongsTo('App\Data\Competencia\Competencia', 'competencia_id');
    }
}
