<?php

namespace App\Data\Nacionalidade;

use Illuminate\Database\Eloquent\Model;

class Nacionalidade extends Model
{
    use Relationship;

    protected $table = 'nacionalidades';
}
