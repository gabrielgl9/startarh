<?php

namespace App\Data\Nacionalidade;

trait Relationship
{
    public function candidatos()
    {
        return $this->hasMany('App\Data\Candidato\Candidato', 'candidato_id');
    }
}
