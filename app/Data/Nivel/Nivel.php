<?php

namespace App\Data\Nivel;

use Illuminate\Database\Eloquent\Model;

class Nivel extends Model
{
    use Relationship;
    
    protected $table = 'niveis';

    protected $fillable = [
        'nome'
    ];
}
