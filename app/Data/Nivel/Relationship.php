<?php

namespace App\Data\Nivel;

trait Relationship
{
    public function formacoes()
    {
        return $this->hasMany('App\Data\Formacao\Formacao', 'nivel_id');
    }
}
