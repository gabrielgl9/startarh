<?php

namespace App\Data\Profissao;

use Illuminate\Database\Eloquent\Model;

class Profissao extends Model
{
    protected $table = 'profissoes';

    protected $fillable = [
        'nome'
    ];
}
