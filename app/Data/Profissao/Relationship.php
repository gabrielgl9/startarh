<?php 

namespace App\Data\Profissao\Profissao;

trait Relationship
{
    public function experiencias()
    {
        return $this->hasMany('App\Data\Experiencia\Experiencia', 'experiencia_id');
    }
}
