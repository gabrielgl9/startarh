<?php

namespace App\Data\Regiao;

use Illuminate\Database\Eloquent\Model;

class Regiao extends Model
{
    use Relationship;

    protected $table = 'regioes';
}
