<?php

namespace App\Data\Regiao;

trait Relationship
{
    public function estados()
    {
        return $this->hasMany('App\Data\Estado\Estado', 'estado_id');
    }
}
