<?php

namespace App\Data\Status;

trait Relationship
{
    public function formacoes()
    {
        return $this->hasMany('App\Data\Formacao\Formacao', 'status_id');
    }
}
