<?php

namespace App\Data\Status;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use Relationship;
    
    protected $table = 'niveis';

    protected $fillable = [
        'nome'
    ];
}
