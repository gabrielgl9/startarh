<?php

namespace App\Http\Controllers;

use App\Data\Candidato\Candidato;
use App\Data\Endereco\Endereco;
use App\Http\Requests\CandidatoRequest;
use Illuminate\Support\Facades\DB;

class CandidatoController extends Controller
{
    
    public function index()
    {
        $candidatos = Candidato::all();
        $candidatos = collect($candidatos)->map(function($candidato){
            return [
                'candidato' => collect($candidato)->except(['endereco']),
                'endereco' => $candidato->endereco
            ];
        })->toArray();
        
        return response()->json($candidatos, 201);
    }

    public function show(Candidato $candidato)
    {
        $candidato =  [
            'candidato' => collect($candidato)->except(['endereco']),
            'endereco' => $candidato->endereco
        ];
        
        return response()->json($candidato, 201);
    }

    public function store(CandidatoRequest $request)
    {
        DB::beginTransaction();

        try {
            $endereco = Endereco::create($request->endereco);

            $candidato = Candidato::create(
                array_merge(
                    $request->dados,
                    ['nacionalidade_id' => $request->nacionalidade_id,
                    'endereco_id' => $endereco->id]
                )
            );

            DB::commit();
            return response()->json($candidato, 201);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['Failed' => 'Erro ao cadastrar um novo candidato.'], 401);
        }
    }

    public function update(CandidatoRequest $request, Candidato $candidato)
    {
        DB::beginTransaction();

        try {
            $candidato->endereco->update($request->endereco);
            
            $candidato->update(
                array_merge(
                    $request->dados,
                    ['nacionalidade_id' => $request->nacionalidade_id,
                    'endereco_id' => $candidato->endereco['id']]
                )
            );

            DB::commit();       
            return response()->json($candidato, 201);   
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['Failed' => 'Erro ao editar um candidato.'], 401);   
        }
    }

    public function destroy(Candidato $candidato)
    {
        DB::beginTransaction();

        try {
            $candidato->endereco()->first()->delete(); 
            DB::commit();       
            return response()->json(['Success' => 'Sucesso ao deletar um candidato.'], 201);   
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['Failed' => 'Erro ao deletar um candidato.'], 401);   
        }
    }
}
