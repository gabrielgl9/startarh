<?php

namespace App\Http\Controllers;

use App\Data\Candidato\Candidato;
use App\Data\Competencia\Competencia;
use App\Data\Competencia\Resource;
use App\Data\Habilidade\Habilidade;
use App\Data\Link\Link;
use App\Http\Requests\CompetenciaRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompetenciaController extends Controller
{
    private $successStatus = 201;
    private $failedStatus = 401;

    /**
     * Cadastra uma nova competência
     * 
     */
    public function store(CompetenciaRequest $request)
    {
        DB::beginTransaction();
        
        try {
            $habilidade = Habilidade::firstOrCreate(
                ['nome' => $request->habilidade],
                ['nome' => $request->habilidade]
            );
            
            $competencia = Competencia::create([
                'nivel' => $request->nivel, 
                'anos_experiencia' => $request->anos_experiencia,
                'habilidade_id' => $habilidade->id,
                'candidato_id' => $request->candidato_id
            ]);

            Link::create([
                'url' => $request->link,
                'competencia_id' => $competencia->id
            ]);

            DB::commit();
            return response()->json(['Success' => $competencia], $this->successStatus);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['Failed' => $e->getMessage()], $this->failedStatus);
        }
    }

    /**
     * Edita uma competência
     * 
     */
    public function update(Competencia $competencia, CompetenciaRequest $request)
    {
        DB::beginTransaction();

        try {
            $habilidade = Habilidade::firstOrCreate(
                ['nome' => $request->habilidade],
                ['nome' => $request->habilidade]
            );
            
            $competencia->update([
                'nivel' => $request->nivel, 
                'anos_experiencia' => $request->anos_experiencia,
                'habilidade_id' => $habilidade->id,
                'candidato_id' => $request->candidato_id
            ]);

            Link::updateOrCreate(
                ['url' => $request->link,
                'competencia_id' => $competencia->id],
                ['url' => $request->link,
                'competencia_id' => $competencia->id]
            );

            DB::commit();
            return response()->json(['Success' => $competencia], $this->successStatus);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json(['Failed' => $e->getMessage()], $this->failedStatus);
        }
    }

    /**
     * Deleta uma competência
     * 
     */
    public function destroy(Competencia $competencia)
    {
        DB::beginTransaction();

        try {
            $competencia->delete();
            DB::commit();
            return response()->json('Sucesso na remoção dessa competência. ', 201);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['Failed' => $e->getMessage()], 401);
        }
    }

    /**
     * Lista as competências de um candidato
     * 
     */
    public function listByCandidato(Candidato $candidato)
    {
        $competencias = Resource::collection($candidato->competencias);
        
        return response()->json($competencias, $this->successStatus);
    }
}
