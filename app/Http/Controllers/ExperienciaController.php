<?php

namespace App\Http\Controllers;

use App\Data\Candidato\Candidato;
use App\Data\Empresa\Empresa;
use App\Data\Experiencia\Experiencia;
use App\Data\Experiencia\Resource;
use App\Data\Profissao\Profissao;
use App\Http\Requests\ExperienciaRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExperienciaController extends Controller
{
    private $successStatus = 201;
    private $failedStatus = 401;

    /**
     * Cadastra uma nova experiência profissional
     * 
     */
    public function store(ExperienciaRequest $request)
    {
        DB::beginTransaction();

        try {
            $empresa = Empresa::firstOrCreate(
                ['nome' => $request->empresa],
                ['nome' => $request->empresa]
            );

            $profissao = Profissao::firstOrCreate(
                ['nome' => $request->profissao],
                ['nome' => $request->profissao]
            );
            
            $experiencia = Experiencia::create([
                'data_inicial' => $request->data_inicial,
                'data_final' => $request->data_final,
                'status' => $request->status,
                'observacao' => $request->observacao,
                'profissao_id' => $profissao->id,
                'empresa_id' => $empresa->id,
                'candidato_id' => $request->candidato_id,
            ]);

            DB::commit();
            return response()->json(['success' => $experiencia], $this->successStatus);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['Failed' => $e->getMessage()], $this->failedStatus);
        }
    }

    /**
     * Edita uma experiência profissional
     * 
     */
    public function update(Experiencia $experiencia, ExperienciaRequest $request)
    {
        DB::beginTransaction();

        try {
            $empresa = Empresa::firstOrCreate(
                ['nome' => $request->empresa],
                ['nome' => $request->empresa]
            );

            $profissao = Profissao::firstOrCreate(
                ['nome' => $request->profissao],
                ['nome' => $request->profissao]
            );
            
            $experiencia->update([
                'data_inicial' => $request->data_inicial,
                'data_final' => $request->data_final,
                'status' => $request->status,
                'observacao' => $request->observacao,
                'profissao_id' => $profissao->id,
                'empresa_id' => $empresa->id,
                'candidato_id' => $request->candidato_id,
            ]);

            DB::commit();
            return response()->json(['success' => $experiencia], $this->successStatus);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['Failed' => $e->getMessage()], $this->failedStatus);
        }
    }

    /**
     * Deleta uma experiência profissional
     * 
     */
    public function destroy(Experiencia $experiencia)
    {
        DB::beginTransaction();

        try {
            $experiencia->delete();
            DB::commit();           
            return response()->json('Sucesso ao remover a experiência solicitada. ', $this->successStatus);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['Failed' => $e->getMessage()], $this->failedStatus);
        }
    }

    /**
     * Lista experiências profissionais de um candidato
     * 
     */
    public function listByCandidato(Candidato $candidato)
    {
        $experiencia = $candidato->experiencias;

        return response()->json(Resource::collection($experiencia) , $this->successStatus);
    }
}
