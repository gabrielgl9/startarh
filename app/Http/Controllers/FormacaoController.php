<?php

namespace App\Http\Controllers;

use App\Data\Area\Area;
use App\Data\Candidato\Candidato;
use App\Data\Formacao\Formacao;
use App\Data\Formacao\Resource;
use App\Data\Instituicao\Instituicao;
use App\Http\Requests\FormacaoRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormacaoController extends Controller
{
    private $successStatus = 201;
    private $failedStatus = 401;

    /**
     * Cadastra uma nova formação
     * 
     */
    public function store(FormacaoRequest $request)
    {
        DB::beginTransaction();

        try {
            $area = Area::firstOrCreate(
                ['nome' => $request->area],
                ['nome' => $request->area]
            );
    
            $instituicao = Instituicao::firstOrCreate(
                ['nome' => $request->instituicao],
                [
                 'nome' => $request->instituicao,
                 'sigla' => $request->sigla
                ]
            );
            
            $formacao = Formacao::create([
                'data_inicial' => $request->data_inicial,
                'data_final' => $request->data_final,
                'status_id' => $request->status_id,
                'area_id' => $area->id,
                'instituicao_id' => $instituicao->id,
                'nivel_id' => $request->nivel_id,
                'candidato_id' => $request->candidato_id
            ]);

            DB::commit();    
            return response()->json($formacao, $this->successStatus);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json('Falha ao tentar cadastrar a formação', $this->failedStatus);
        }
    }

    /**
     * Lista as formações a partir de um candidato informado
     * 
     */
    public function listarPorCandidato(Candidato $candidato)
    {
        $formacoes = $candidato->formacoes;

        return response()->json(Resource::collection($formacoes), 201);
    }

    /**
     * Edita uma formação
     * 
     */
    public function update(Formacao $formacao, FormacaoRequest $request)
    {
        DB::beginTransaction();

        try {
            
            $area = Area::firstOrCreate(
                ['nome' => $request->area],
                ['nome' => $request->area]
            );
    
            $instituicao = Instituicao::firstOrCreate(
                ['nome' => $request->instituicao],
                [
                 'nome' => $request->instituicao,
                 'sigla' => $request->sigla
                ]
            );
            
            $formacao->update([
                'data_inicial' => $request->data_inicial,
                'data_final' => $request->data_final,
                'status_id' => $request->status_id,
                'area_id' => $area->id,
                'instituicao_id' => $instituicao->id,
                'nivel_id' => $request->nivel_id,
                'candidato_id' => $request->candidato_id
            ]);

            DB::commit();
            return response()->json(['success' => $formacao], 201);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json('Falha ao tentar fazer upload da formação: '. $e->getMessage(), 401);
        }
    }

    /**
     * Deleta uma formação
     * 
     */
    public function destroy(Formacao $formacao)
    {
        DB::beginTransaction();

        try {
            $formacao->delete();
            
            DB::commit();
            return response()->json('Formação removida com sucesso.', 201);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json('Falha ao tentar remover uma formação.', 401);
        }
    }
}
