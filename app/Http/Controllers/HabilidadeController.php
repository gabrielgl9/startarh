<?php

namespace App\Http\Controllers;

use App\Data\Habilidade\Habilidade;
use App\Data\Habilidade\Resource;
use Illuminate\Http\Request;

class HabilidadeController extends Controller
{
    /**
     * Lista todas as habilidades existentes no sistema
     * 
     */
    public function list()
    {
        $habilidades = Habilidade::all();

        return response()->json(Resource::collection($habilidades), 201);
    }
}
