<?php

namespace App\Http\Controllers;

use App\Data\Nivel\Nivel;
use App\Data\Nivel\Resource;

class NivelController extends Controller
{
    /**
     * Lista todos os níveis existentes
     * 
     */
    public function list()
    {
        $niveis = Nivel::all();

        return response()->json(Resource::collection($niveis), 201);
    }
}
