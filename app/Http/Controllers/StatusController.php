<?php

namespace App\Http\Controllers;

use App\Data\Status\Resource;
use App\Data\Status\Status;

class StatusController extends Controller
{
    public function list()
    {
        $statuses = Status::all();
        
        return response()->json(Resource::collection($statuses), 201);
    }
}
