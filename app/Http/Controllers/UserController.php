<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\User;

class UserController extends Controller
{
    public function register(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            $request['password'] = Hash::make($request->password);
            $newUser = User::create($request->all());
            Auth::login($newUser);
            $tokenResult = Auth::user()->createToken('Personal Access Token');   
            
            DB::commit();
            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
            ], 201);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json(['Failed' => $e->getMessage()], 201);
        }
    }
    
    public function login(LoginRequest $request)
    {
        $credenciais = $request->only(['email', 'password']);
        if (Auth::attempt($credenciais)) {
            $tokenResult = Auth::user()->createToken('Personal Access Token');   
            
            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
            ], 201);
        }
        return response()->json(['Erro ao tentar autenticar com essas credenciais'], 401);
    }

    public function logout()
    {
        Auth::user()->token()->revoke();

        return response()->json([
            'message' => 'Deslogado com suceso.'
        ]);
    }
}
