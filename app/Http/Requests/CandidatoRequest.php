<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidatoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Dados para a tabela Candidatos
            'dados.nome' => 'required|string',
            'dados.data_nascimento' => 'required|string',
            'dados.email' => 'required|string|email',
            'dados.telefone' => 'required|string',
            'dados.naturalidade' => 'required|string',
            // Dados para a tabela Enderecos
            'endereco.cep' => 'required|string',
            'endereco.estado_id' => 'required|numeric',
            'endereco.cidade_id' => 'required|numeric',
            'endereco.bairro' => 'required|string',
            'endereco.logradouro' => 'required|string',
            'endereco.numero' => 'required|numeric',
            'endereco.complemento' => 'string',
            // Dados para a tabela Nacionalidades
            'nacionalidade_id' => 'required|numeric',
        ];
    }
}
