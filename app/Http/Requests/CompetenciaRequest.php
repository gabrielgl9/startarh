<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompetenciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nivel" => "required|string|in:trainee,junior,pleno,senior",
            "anos_experiencia" => 'required|numeric',
            "habilidade" => "required|string",
            "link" => "required|string|url|max:255",
            "candidato_id" => "required|numeric|exists:candidatos,id"
        ];
    }
}
