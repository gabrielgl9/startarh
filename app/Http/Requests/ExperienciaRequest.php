<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExperienciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "data_inicial" => "required|date",
            "data_final" => "required|date",
            "status" => "required|string|in:passado,presente",
            "observacao" => "required|string",
            "profissao" => "required|string",
            "empresa" => "required|string",
            "candidato_id" => "required|numeric|exists:candidatos,id"
        ];
    }
}
