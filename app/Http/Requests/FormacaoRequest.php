<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormacaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "data_inicial" => "required|date",
            "data_final" => "required|date",
            "status_id" => "required|numeric|exists:status,id",
            "area" => "required|string",
            "instituicao" => "required|string",
            "sigla" => "string|max:15",
            "nivel_id" => "required|numeric|exists:niveis,id",
            "candidato_id" => "required|numeric|exists:candidatos,id"
        ];
    }
}
