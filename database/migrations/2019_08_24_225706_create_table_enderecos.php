<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEnderecos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enderecos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cep', 20);
            $table->string('logradouro', 255);
            $table->string('bairro', 255);
            $table->integer('numero');
            $table->string('complemento', 255)->nullable();
            $table->enum('tipo', ['residencial', 'comercial']);
            $table->unsignedInteger('cidade_id');
            $table->foreign('cidade_id')
                        ->references('id')
                        ->on('cidades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
    }
}
