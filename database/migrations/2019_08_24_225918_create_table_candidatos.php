<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCandidatos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidatos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email')->unique();
            $table->string('telefone');
            $table->date('data_nascimento');
            $table->string('naturalidade');
            $table->unsignedInteger('nacionalidade_id');
            $table->foreign('nacionalidade_id')
                        ->references('id')
                        ->on('nacionalidades');
            $table->unsignedInteger('endereco_id');
            $table->foreign('endereco_id')
                        ->references('id')
                        ->on('enderecos')
                        ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidatos');
    }
}
