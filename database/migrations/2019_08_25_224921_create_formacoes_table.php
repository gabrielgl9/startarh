<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_inicial');
            $table->date('data_final');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('area_id');
            $table->unsignedInteger('instituicao_id');
            $table->unsignedInteger('nivel_id');
            $table->unsignedInteger('candidato_id');
            $table->foreign('area_id')
                    ->references('id')
                    ->on('areas');
            $table->foreign('instituicao_id')
                    ->references('id')
                    ->on('instituicoes');
            $table->foreign('nivel_id')
                    ->references('id')
                    ->on('niveis');
            $table->foreign('status_id')
                    ->references('id')
                    ->on('status');
            $table->foreign('candidato_id')
                    ->references('id')
                    ->on('candidatos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formacoes');
    }
}
