<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExperiencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiencias', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_inicial');
            $table->date('data_final')->nullable();
            $table->enum('status', ['presente', 'passado'])->comment('Trabalha atualmente/trabalhou');
            $table->text('observacao');
            $table->unsignedInteger('profissao_id');
            $table->unsignedInteger('empresa_id');
            $table->unsignedInteger('candidato_id');
            $table->foreign('profissao_id')
                    ->references('id')
                    ->on('profissoes')
                    ->onDelete('cascade');
            $table->foreign('empresa_id')
                    ->references('id')
                    ->on('empresas')
                    ->onDelete('cascade');
            $table->foreign('candidato_id')
                    ->references('id')
                    ->on('candidatos')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiencias');
    }
}
