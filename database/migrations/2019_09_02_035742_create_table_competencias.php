<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompetencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competencias', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('nivel', ['trainee', 'junior', 'pleno', 'senior']);
            $table->float('anos_experiencia', 8, 2);
            $table->unsignedInteger('candidato_id');
            $table->unsignedInteger('habilidade_id');
            $table->foreign('candidato_id')
                            ->references('id')
                            ->on('candidatos')
                            ->onDelete('cascade');
            $table->foreign('habilidade_id')
                            ->references('id')
                            ->on('habilidades')
                            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competencias');
    }
}
