<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(NacionalidadesTableSeeder::class);
        $this->call(RegioesTableSeeder::class);
        $this->call(EstadosTableSeeder::class);
        $this->call(CidadesTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(NiveisTableSeeder::class);
    }
}
