<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('estados')->delete();
        
        \DB::table('estados')->insert(array (
            0 => 
            array (
                'id' => 1,
                'codigo_ibge' => 12,
                'nome' => 'Acre',
                'uf' => 'AC',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'codigo_ibge' => 27,
                'nome' => 'Alagoas',
                'uf' => 'AL',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'codigo_ibge' => 16,
                'nome' => 'Amapá',
                'uf' => 'AP',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'codigo_ibge' => 13,
                'nome' => 'Amazonas',
                'uf' => 'AM',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'codigo_ibge' => 29,
                'nome' => 'Bahia',
                'uf' => 'BA',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 2,
            ),
            5 => 
            array (
                'id' => 6,
                'codigo_ibge' => 23,
                'nome' => 'Ceará',
                'uf' => 'CE',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 2,
            ),
            6 => 
            array (
                'id' => 7,
                'codigo_ibge' => 53,
                'nome' => 'Distrito Federal',
                'uf' => 'DF',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 5,
            ),
            7 => 
            array (
                'id' => 8,
                'codigo_ibge' => 32,
                'nome' => 'Espírito Santo',
                'uf' => 'ES',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 3,
            ),
            8 => 
            array (
                'id' => 9,
                'codigo_ibge' => 52,
                'nome' => 'Goiás',
                'uf' => 'GO',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 5,
            ),
            9 => 
            array (
                'id' => 10,
                'codigo_ibge' => 21,
                'nome' => 'Maranhão',
                'uf' => 'MA',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 2,
            ),
            10 => 
            array (
                'id' => 11,
                'codigo_ibge' => 51,
                'nome' => 'Mato Grosso',
                'uf' => 'MT',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 5,
            ),
            11 => 
            array (
                'id' => 12,
                'codigo_ibge' => 50,
                'nome' => 'Mato Grosso do Sul',
                'uf' => 'MS',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 5,
            ),
            12 => 
            array (
                'id' => 13,
                'codigo_ibge' => 31,
                'nome' => 'Minas Gerais',
                'uf' => 'MG',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 3,
            ),
            13 => 
            array (
                'id' => 14,
                'codigo_ibge' => 15,
                'nome' => 'Pará',
                'uf' => 'PA',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'codigo_ibge' => 25,
                'nome' => 'Paraíba',
                'uf' => 'PB',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 2,
            ),
            15 => 
            array (
                'id' => 16,
                'codigo_ibge' => 41,
                'nome' => 'Paraná',
                'uf' => 'PR',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 4,
            ),
            16 => 
            array (
                'id' => 17,
                'codigo_ibge' => 26,
                'nome' => 'Pernambuco',
                'uf' => 'PE',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 2,
            ),
            17 => 
            array (
                'id' => 18,
                'codigo_ibge' => 22,
                'nome' => 'Piauí',
                'uf' => 'PI',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 2,
            ),
            18 => 
            array (
                'id' => 19,
                'codigo_ibge' => 33,
                'nome' => 'Rio de Janeiro',
                'uf' => 'RJ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 3,
            ),
            19 => 
            array (
                'id' => 20,
                'codigo_ibge' => 24,
                'nome' => 'Rio Grande do Norte',
                'uf' => 'RN',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 2,
            ),
            20 => 
            array (
                'id' => 21,
                'codigo_ibge' => 43,
                'nome' => 'Rio Grande do Sul',
                'uf' => 'RS',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 4,
            ),
            21 => 
            array (
                'id' => 22,
                'codigo_ibge' => 11,
                'nome' => 'Rondônia',
                'uf' => 'RO',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 1,
            ),
            22 => 
            array (
                'id' => 23,
                'codigo_ibge' => 14,
                'nome' => 'Roraima',
                'uf' => 'RR',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 1,
            ),
            23 => 
            array (
                'id' => 24,
                'codigo_ibge' => 42,
                'nome' => 'Santa Catarina',
                'uf' => 'SC',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 4,
            ),
            24 => 
            array (
                'id' => 25,
                'codigo_ibge' => 35,
                'nome' => 'São Paulo',
                'uf' => 'SP',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 3,
            ),
            25 => 
            array (
                'id' => 26,
                'codigo_ibge' => 28,
                'nome' => 'Sergipe',
                'uf' => 'SE',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 2,
            ),
            26 => 
            array (
                'id' => 27,
                'codigo_ibge' => 17,
                'nome' => 'Tocantins',
                'uf' => 'TO',
                'created_at' => NULL,
                'updated_at' => NULL,
                'regiao_id' => 1,
            ),
        ));
    }
}
