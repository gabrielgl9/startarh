<?php

use Illuminate\Database\Seeder;

class NiveisTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('niveis')->delete();
        
        \DB::table('niveis')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nome' => 'Ensino Fundamental',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nome' => 'Ensino Médio',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nome' => 'Curso Profissionalizante',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nome' => 'Curso Técnico',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nome' => 'Ensino Superior',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nome' => 'Pós Graduação/MBA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nome' => 'Mestrado',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nome' => 'Doutorado/Pós doutorado',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}