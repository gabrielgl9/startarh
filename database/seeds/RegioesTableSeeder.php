<?php

use Illuminate\Database\Seeder;

class RegioesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('regioes')->delete();
        
        \DB::table('regioes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nome' => 'Norte',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nome' => 'Nordeste',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nome' => 'Sudeste',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nome' => 'Sul',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nome' => 'Centro-Oeste',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));

    }
}
