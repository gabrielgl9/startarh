<?php

Route::prefix('v1')->group(function(){
    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');
});

Route::prefix('v1')->middleware('auth:api')->group(function() {
    // User
    Route::get('logout', 'UserController@logout');
    // Candidatos
    Route::get('listar-candidatos', 'CandidatoController@index');
    Route::get('listar-candidato/{candidato}', 'CandidatoController@show');
    Route::post('cadastrar-candidato', 'CandidatoController@store');
    Route::delete('remover-candidato/{candidato}', 'CandidatoController@destroy');
    Route::put('editar-candidato/{candidato}', 'CandidatoController@update');
    // Formações
    Route::post('cadastrar-formacao', 'FormacaoController@store');
    Route::get('listar-formacoes-candidato/{candidato}', 'FormacaoController@listarPorCandidato');
    Route::put('editar-formacao/{formacao}', 'FormacaoController@update');
    Route::delete('remover-formacao/{formacao}', 'FormacaoController@destroy');
    // Listagens de Formações
    Route::get('listar-status', 'StatusController@list');
    Route::get('listar-niveis', 'NivelController@list');
    // Experiências profissionais
    Route::post('cadastrar-experiencia', 'ExperienciaController@store');
    Route::put('editar-experiencia/{experiencia}', 'ExperienciaController@update');
    Route::get('listar-experiencias-candidato/{experiencia}', 'ExperienciaController@listByCandidato');
    Route::delete('remover-experiencia/{experiencia}', 'ExperienciaController@destroy');
    // Habilidades
    Route::get('listar-habilidades', 'HabilidadeController@list');
    // Competências
    Route::post('cadastrar-competencia','CompetenciaController@store');
    Route::put('editar-competencia/{competencia}', 'CompetenciaController@update');
    Route::delete('deletar-competencia/{competencia}', 'CompetenciaController@destroy');
    Route::get('listar-competencias-candidato/{candidato}', 'CompetenciaController@listByCandidato');
});

